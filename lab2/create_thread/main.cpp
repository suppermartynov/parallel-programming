#include <iostream>
#include <fstream>
#include <windows.h>
#include <cmath>
#include <chrono>

using namespace std;

struct ThreadData {
    char* buffer;
    int leftBorder;
    int rightBorder;
    int x;
};

DWORD WINAPI ThreadFunc(void* pData) {
    ThreadData* threadData = static_cast<ThreadData*>(pData);
    for (int i = threadData->rightBorder; i > threadData->leftBorder; i--) {
        threadData->buffer[i] += (i * threadData->x) & 255;
    }
    return 0;
}

int main() {
    auto start_time = chrono::high_resolution_clock::now();

    const int x = 55;
    const int THREADS_AMOUNT = 13;

    FILE* fp = fopen("../pict.png", "rb");

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* buffer = new char[file_size / sizeof(char)];

    fread(buffer, 1, file_size, fp);

    double interval_size = ceil((double)file_size / THREADS_AMOUNT);

    HANDLE threads[THREADS_AMOUNT];
    ThreadData threadDataArray[THREADS_AMOUNT];

    for (int i = 0; i < THREADS_AMOUNT; i++) {
        int end = max(0, static_cast<int>(file_size - i * interval_size));
        int start = max(0, static_cast<int>(end - interval_size));

        threadDataArray[i].buffer = buffer;
        threadDataArray[i].leftBorder = start;
        threadDataArray[i].rightBorder = end;
        threadDataArray[i].x = x;

        threads[i] = CreateThread(NULL, 0, ThreadFunc, &threadDataArray[i], 0, NULL);
    }

    WaitForMultipleObjects(THREADS_AMOUNT, threads, TRUE, INFINITE);

    for (int i = 0; i < THREADS_AMOUNT; i++) {
        CloseHandle(threads[i]);
    }

    fclose(fp);

    ofstream output_file("output.png", ios::binary);

    output_file.write(buffer, file_size);

    auto end_time = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(end_time - start_time);
    cout << "Время выполнения: " << duration.count() << " микросекунд" << endl;

    return 0;
}
