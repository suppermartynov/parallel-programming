#include <iostream>
#include <fstream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <thread>
#include <chrono>

using namespace std;

void changeFile(char *buffer, int leftBorder, int rightBorder, int x) {
    for (int i = rightBorder; i > leftBorder; i--) {
        buffer[i] += (i * x) & 255;
    }
}

int main() {
    auto start_time = chrono::high_resolution_clock::now();

    const int x = 55;
    const int THREADS_AMOUNT = 13;

    FILE* fp = fopen("../pict.png", "rb");

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* buffer = new char[file_size / sizeof(char)];

    fread(buffer, 1, file_size, fp);

    double interval_size = ceil((double) file_size / THREADS_AMOUNT);

    thread threads[THREADS_AMOUNT];

    for (int i = 0; i < THREADS_AMOUNT; i++) {
        int end = max(0, (int)(file_size - i * interval_size));
        int start = max(0, (int)(end - interval_size));
        threads[i] = thread(changeFile, buffer, start, end, x);
    }

    for (int i = 0; i < THREADS_AMOUNT; i++) {
        threads[i].join();
    }

    fclose(fp);

    ofstream output_file("output.png", ios::binary);

    output_file.write(buffer, file_size);

    auto end_time = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(end_time - start_time);
    cout << "Время выполнения: " << duration.count() << " микросекунд" << std::endl;

    return 0;
}