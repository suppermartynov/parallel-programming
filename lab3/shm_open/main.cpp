#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <chrono>

using namespace std;

struct ThreadData {
    char* buffer;
    int leftBorder;
    int rightBorder;
    int x;
};

void* changeFile(void* data) {
    ThreadData* threadData = static_cast<ThreadData*>(data);
    for (int i = threadData->rightBorder; i > threadData->leftBorder; i--) {
        threadData->buffer[i] += (i * threadData->x) & 256;
    }
    return nullptr;
}

int main() {
    auto start_time = chrono::high_resolution_clock::now();

    const int x = 55;
    const int THREADS_AMOUNT = 13;
    const char* SHARED_MEMORY_NAME = "/my_shared_memory";

    FILE* fp = fopen("../pict.png", "rb");

    if (fp == nullptr) {
        cerr << "Ошибка открытия файла." << endl;
        return 1;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    int shm_fd = shm_open(SHARED_MEMORY_NAME, O_CREAT | O_RDWR, 0666);

    ftruncate(shm_fd, file_size);

    char* buffer = static_cast<char*>(mmap(nullptr, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0));

    fread(buffer, 1, file_size, fp);

    for (int i = 0; i < THREADS_AMOUNT; i++) {
        pid_t pid = fork();

        if (pid == 0) {
            int end = max(0, static_cast<int>(file_size - i * file_size / THREADS_AMOUNT));
            int start = max(0, static_cast<int>(end - file_size / THREADS_AMOUNT));

            ThreadData threadData;
            threadData.buffer = buffer;
            threadData.leftBorder = start;
            threadData.rightBorder = end;
            threadData.x = x;

            changeFile(&threadData);

            munmap(buffer, file_size);
            close(shm_fd);

            _exit(0);
        } else if (pid < 0) {
            cerr << "Ошибка fork" << endl;
            munmap(buffer, file_size);
            close(shm_fd);
            fclose(fp);
            return 1;
        }
    }
    for (int i = 0; i < THREADS_AMOUNT; i++) {
        wait(nullptr);
    }

    ofstream output_file("output.png", ios::binary);
    output_file.write(buffer, file_size);

    munmap(buffer, file_size);
    close(shm_fd);

    fclose(fp);

    return 0;
}
