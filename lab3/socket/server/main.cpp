#include <iostream>
#include <unistd.h>
#include <arpa/inet.h>
#include <vector>

const int PORT = 8080;
const int CHUNK_SIZE = 4096;

void processChunk(char* buffer, ssize_t size) {
    const int x = 55;
    for (int i = 0; i < size; i++) {
        buffer[i] += (i * x) & 255;
    }
}

int main() {
    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = INADDR_ANY;

    if (bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
        perror("Ошибка при привязке сокета");
        close(serverSocket);
        return 1;
    }

    if (listen(serverSocket, 13) == -1) {
        perror("Ошибка при прослушивании сокета");
        close(serverSocket);
        return 1;
    }

    std::cout << "Сервер ожидает подключения..." << std::endl;

    while (true) {
        int clientSocket = accept(serverSocket, NULL, NULL);
        if (clientSocket == -1) {
            perror("Ошибка при принятии подключения");
            continue;
        }

        std::cout << "Клиент подключен!" << std::endl;

        // Принимаем размер данных от клиента
        size_t dataSize;
        recv(clientSocket, &dataSize, sizeof(dataSize), 0);


        size_t totalBytesReceived = 0;

        std::cout << "Обрабатываю присланный буффер" << std::endl;
        std::vector<char> processedData;

        while (totalBytesReceived < dataSize) {
            char buffer[CHUNK_SIZE];
            ssize_t bytesRead = recv(clientSocket, buffer, sizeof(buffer), 0);
            if (bytesRead > 0) {
                processChunk(buffer, bytesRead);
                processedData.insert(processedData.end(), buffer, buffer + bytesRead);
                totalBytesReceived += bytesRead;
            }
        }

        std::cout << "Возвращаю данные обратно" << std::endl;
        send(clientSocket, processedData.data(), processedData.size(), 0);

        processedData.clear();
        close(clientSocket);
    }

    close(serverSocket);

    return 0;
}
