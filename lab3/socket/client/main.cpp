#include <iostream>
#include <unistd.h>
#include <arpa/inet.h>
#include <fstream>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <cmath>
#include <cstring>
#include <mutex>

using namespace std;

const int PORT = 8080;
const char* SERVER_IP = "127.0.0.1";
const char* SHM_NAME = "/my_shared_memory";

int main() {
    std::ifstream inputFile("../pict.png", std::ios::binary);

    inputFile.seekg(0, std::ios::end);
    long file_size = inputFile.tellg();
    inputFile.seekg(0, std::ios::beg);

    const int PROCESS_AMOUNT = 13;

    int shm_fd = shm_open(SHM_NAME, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    ftruncate(shm_fd, file_size);

    char* buffer = static_cast<char*>(mmap(nullptr, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0));
    close(shm_fd);

    inputFile.read(buffer, file_size);

    double interval_size = ceil((double)file_size / PROCESS_AMOUNT);

    for (int i = 0; i < PROCESS_AMOUNT; i++) {
        pid_t pid = fork();

        if (pid == 0) {
            int end = max(0, static_cast<int>(file_size - i * interval_size));
            int start = max(0, static_cast<int>(end - interval_size));

            int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
            struct sockaddr_in serverAddr;
            serverAddr.sin_family = AF_INET;
            serverAddr.sin_port = htons(PORT);
            inet_pton(AF_INET, SERVER_IP, &(serverAddr.sin_addr));

            if (connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) < 0) {
                perror("Ошибка при подключении");
                close(clientSocket);
                exit(EXIT_FAILURE);
            }

            // Отправляем размер данных перед данными
            ssize_t dataSize = end - start;
            send(clientSocket, &dataSize, sizeof(dataSize), 0);

            // После этого отправляем сами данные
            ssize_t bytesSent = send(clientSocket, &buffer[start], dataSize, 0);

            char* responseBuffer = new char[dataSize];

            // Принимаем сами данные
            int bytesRead = recv(clientSocket, responseBuffer, bytesSent, 0);

            cout << "Поток " << getpid() << ". Количество принятых байт с сервера - " << bytesRead << endl;

            int processedBufferSize = end - start;
            printf("Address of the array: %p\n", (void*)buffer);

            memcpy(&buffer[start], responseBuffer, processedBufferSize);

            delete[] responseBuffer;

            close(clientSocket);
            exit(0);
        }
    }

    for (int i = 0; i < PROCESS_AMOUNT; i++) {
        pid_t child_pid = wait(nullptr);
    }

    int output_fd = shm_open(SHM_NAME, O_RDONLY, 0);
    char* output_buffer = static_cast<char*>(mmap(nullptr, file_size, PROT_READ, MAP_SHARED, output_fd, 0));
    close(output_fd);

    ofstream output_file("output.png", ios::binary);
    output_file.write(output_buffer, file_size);

    output_file.close();
    inputFile.close();

    shm_unlink(SHM_NAME);

    return 0;
}
