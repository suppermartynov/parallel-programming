#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cmath>
#include <chrono>
#include <cstring>

using namespace std;

struct ThreadData {
    char* buffer;
    int leftBorder;
    int rightBorder;
    int x;
};

void changeFile(ThreadData* threadData) {
    for (int i = threadData->rightBorder; i > threadData->leftBorder; i--) {
        threadData->buffer[i] += (i * threadData->x) & 255;
    }
}

int main() {
    auto start_time = chrono::high_resolution_clock::now();

    const int x = 55;
    const int PROCESS_AMOUNT = 13;

    int fd = open("../pict.png", O_RDWR);

    struct stat sb;
    fstat(fd, &sb);
    long file_size = sb.st_size;

    char* buffer = static_cast<char*>(mmap(nullptr, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
    close(fd);

    double interval_size = ceil((double)file_size / PROCESS_AMOUNT);

    for (int i = 0; i < PROCESS_AMOUNT; i++) {
        pid_t pid = fork();

        if (pid == 0) {

            int end = max(0, static_cast<int>(file_size - i * interval_size));
            int start = max(0, static_cast<int>(end - interval_size));

            cout << "Дочерний процесс с PID " << getpid() << " начался. \n" << "Интервалы: " << start << " - " << end << endl;

            ThreadData threadData;
            threadData.buffer = buffer;
            threadData.leftBorder = start;
            threadData.rightBorder = end;
            threadData.x = x;

            changeFile(&threadData);

            exit(0);
        }
    }

    for (int i = 0; i < PROCESS_AMOUNT; i++) {
        pid_t child_pid = wait(nullptr);
        cout << "Дочерний процесс с PID " << child_pid << " закончился." << endl;
    }

    ofstream output_file("output.png", ios::binary);
    output_file.write(buffer, file_size);

    auto end_time = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(end_time - start_time);
    cout << "Время выполнения: " << duration.count() << " микросекунд" << endl;

    return 0;
}