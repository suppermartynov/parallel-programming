#include <windows.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

//CreateProcess + SharedMemory

using namespace std;

const int PROCESS_AMOUNT = 13;

int main() {
    SetConsoleOutputCP(CP_UTF8);

    FILE* fp = fopen("../pict.png", "rb");

    if (fp == nullptr) {
        std::cerr << "Ошибка открытия файла." << std::endl;
        return 1;
    }

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* buffer = new char[file_size / sizeof(char)];

    fread(buffer, 1, file_size, fp);

    double interval_size = ceil((double)file_size / PROCESS_AMOUNT);

    HANDLE hMapFile = CreateFileMappingW(
            INVALID_HANDLE_VALUE,
            NULL,
            PAGE_READWRITE,
            0,
            file_size,
            L"MySharedMemory");

    if (hMapFile == NULL) {
        std::cerr << "Не удалось создать объект отображения файла (" << GetLastError() << ")." << std::endl;
        delete[] buffer;
        fclose(fp);
        return 1;
    }

    char* pData = (char*)MapViewOfFile(hMapFile, FILE_MAP_WRITE, 0, 0, file_size);

    if (pData == NULL) {
        std::cerr << "Не удалось отобразить файл (" << GetLastError() << ")." << std::endl;
        CloseHandle(hMapFile);
        delete[] buffer;
        fclose(fp);
        return 1;
    }

    // Копируем буфер в разделяемую память
    memcpy(pData, buffer, file_size);


    const string executablePath = R"(C:\Users\Elena\CLionProjects\new\cmake-build-debug\child.exe)";

    cout << executablePath.c_str() << endl;

    vector<HANDLE> processHandles(PROCESS_AMOUNT);

    for (int i = 0; i < PROCESS_AMOUNT; i++) {
        STARTUPINFO si;
        PROCESS_INFORMATION pi;

        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);
        ZeroMemory(&pi, sizeof(pi));

        int end = max(0, static_cast<int>(file_size - i * interval_size));
        int start = max(0, static_cast<int>(end - interval_size));

        string commandLine = to_string(i) + " " + to_string(start) + " " + to_string(end);

        if (CreateProcess(
                executablePath.c_str(),
                const_cast<char*>(commandLine.c_str()),
                NULL,
                NULL,
                FALSE,
                0,
                NULL,
                NULL,
                &si,
                &pi
        )) {
            processHandles[i] = pi.hProcess;
        } else {
            std::cerr << "CreateProcess завершился с ошибкой " << GetLastError() << std::endl;
            UnmapViewOfFile(pData);
            CloseHandle(hMapFile);
            delete[] buffer;
            fclose(fp);
            return 1;
        }
    }

    WaitForMultipleObjects(PROCESS_AMOUNT, processHandles.data(), TRUE, INFINITE);

    for (int i = 0; i < PROCESS_AMOUNT; i++) {
        CloseHandle(processHandles[i]);
    }

    memcpy(buffer, pData, file_size);
    UnmapViewOfFile(pData);
    CloseHandle(hMapFile);


    ofstream output_file("output.png", ios::binary);
    output_file.write(buffer, file_size);

    delete[] buffer;
    fclose(fp);

    return 0;
}
