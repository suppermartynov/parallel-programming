#include <windows.h>
#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
    SetConsoleOutputCP(CP_UTF8);

    cout << "Поток " << argv[0] << " начал работу" << endl;

    int start = std::stoi(argv[1]);
    int end = std::stoi(argv[2]);

    HANDLE hMapFile = OpenFileMappingW(FILE_MAP_WRITE, FALSE, L"MySharedMemory");

    if (hMapFile == NULL) {
        return 1;
    }

    char* pData = (char*)MapViewOfFile(hMapFile, FILE_MAP_WRITE, 0, 0, 0);

    if (pData == NULL) {
        CloseHandle(hMapFile);
        return 1;
    }

    for (int i = start; i < end; i++) {
        pData[i] += (i * 55) & 255;
    }

    UnmapViewOfFile(pData);
    CloseHandle(hMapFile);

    return 0;
}
