#include <iostream>
#include <fstream>
#include <stdio.h>
#include "chrono"

using namespace std;

int main() {
    auto start_time = std::chrono::high_resolution_clock::now();

    const int x = 55;

    FILE* fp = fopen("../pict.png", "rb");

    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* buffer = new char[file_size / sizeof(char)];

    fread(buffer, 1, file_size, fp);

    for (int i = 0; i < file_size; i++) {
        buffer[i] += (i * x) & 256 ;
    }

    fclose(fp);

    ofstream output_file("output.png", ios::binary);

    output_file.write(buffer, file_size);

    auto end_time = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Время выполнения: " << duration.count() << " микросекунд" << std::endl;

    return 0;
}