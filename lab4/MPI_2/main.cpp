#include <iostream>
#include <fstream>
#include <mpi.h>
#include <cstdlib>

using namespace std;

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int processCount, processNum;
    MPI_Comm_size(MPI_COMM_WORLD, &processCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &processNum);

    if (processNum == 0) {
        FILE* fp;

        if (fopen_s(&fp, "./pict.png", "rb") != 0) {
            cout << "Error opening file." << endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
            return 1;
        }

        fseek(fp, 0L, SEEK_END);
        size_t fileSize = ftell(fp);
        fseek(fp, 0L, SEEK_SET);

        char* data = new char[fileSize];
        fread(data, fileSize, 1, fp);
        fclose(fp);

        size_t chunkSize = ceil((double)fileSize / (processCount - 1));

        for (int process = 1; process < processCount; process++) {
            int start = (process - 1) * chunkSize;
            int end = min(fileSize, start + chunkSize);

            cout << "Borders " << start << " - " << start << " end " << " - " << end << endl;

            MPI_Send(data + start, end - start, MPI_CHAR, process, 0, MPI_COMM_WORLD);
        }

        char* finalBuffer = new char[fileSize];
        cout << "file size " << fileSize;

        for (int process = 1; process < processCount; process++) {
            int start = (process - 1) * chunkSize;
            int end = min(fileSize, start + chunkSize);

            MPI_Recv(finalBuffer + start, end - start, MPI_CHAR, process, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        ofstream output_file("output.png", ios::binary);
        output_file.write(finalBuffer, fileSize);
        output_file.close();

        delete[] data;
    }
    else {
        MPI_Status status;
        int fileSize;

        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_CHAR, &fileSize);

        cout << "Process: " << processNum;

        char* tmpArray = (char*)malloc(fileSize * sizeof(char));
        MPI_Recv(tmpArray, fileSize, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (size_t i = 0; i < fileSize; i++) {
            tmpArray[i] = tmpArray[i] + (i * 55) & 256;
        }

        MPI_Send(tmpArray, fileSize, MPI_CHAR, 0, 0, MPI_COMM_WORLD);

        free(tmpArray);
    }

    MPI_Finalize();
    return 0;
}